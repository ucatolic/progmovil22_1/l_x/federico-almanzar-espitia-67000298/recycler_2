package com.sigmotoa.recycler2

import android.content.res.Resources
import com.sigmotoa.recycler2.R

class Pokemons {
    companion object{
        val pokemonList = listOf<Pokemon>(
            Pokemon(
                id=1,
                name="Bulbasaur",
                type="plant",
                image = R.drawable.pk_001
            ),
            Pokemon(
                id=2,
                name="Ivysaur",
                type="plant",
                image = R.drawable.pk_002
            ),
            Pokemon(
                id=3,
                name="Venusaur",
                type="plant",
                image = R.drawable.pk_003
            ),
            Pokemon(
                id=4,
                name="Charmander",
                type="fire",
                image = R.drawable.pk_004
            ),
            Pokemon(
                id=5,
                name="Charmeleon",
                type="fire",
                image = R.drawable.pk_005
            ),
            Pokemon(
                id=6,
                name="Charizard",
                type="fire",
                image = R.drawable.pk_006
            ),
            Pokemon(
                id=7,
                name="Squartle",
                type="water",
                image = R.drawable.pk_007
            )
        )
    }
}
