package com.sigmotoa.recycler2.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.graphics.drawable.toDrawable
import androidx.recyclerview.widget.RecyclerView
import com.sigmotoa.recycler2.Pokemon
import com.sigmotoa.recycler2.R

class PokemonViewHolder(val view: View): RecyclerView.ViewHolder(view) {

    val pokemonName = view.findViewById<TextView>(R.id.tvPokemonName)
    val pokemonId = view.findViewById<TextView>(R.id.tvPokemonId)
    val pokemonType = view.findViewById<TextView>(R.id.tvType)

    val pokemonPic = view.findViewById<ImageView>(R.id.ivPokemon)

    fun show(pokemonModel: Pokemon)
    {
        pokemonName.text=pokemonModel.name
        pokemonId.text=pokemonModel.id.toString()
        pokemonType.text=pokemonModel.type

        pokemonModel.image?.let { pokemonPic.setImageResource(it) }

    }
}