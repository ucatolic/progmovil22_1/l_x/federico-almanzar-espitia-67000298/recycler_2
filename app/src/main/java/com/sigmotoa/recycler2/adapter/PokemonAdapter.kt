package com.sigmotoa.recycler2.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sigmotoa.recycler2.Pokemon
import com.sigmotoa.recycler2.R

class PokemonAdapter(private val pokemonList:List<Pokemon>): RecyclerView.Adapter<PokemonViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return PokemonViewHolder(layoutInflater.inflate(R.layout.item_pokemon, parent, false))
    }

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        val item = pokemonList[position]
        holder.show(item)
    }

    override fun getItemCount(): Int {
        return pokemonList.size
    }
}